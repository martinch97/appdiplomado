package com.martin.appcocina.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martin.appcocina.db.entities.DetalleOrden;

import java.util.List;

@Dao
public interface DetalleOrdenDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<DetalleOrden> detalleOrdenList);

    @Query("SELECT * FROM detalleOrden")
    List<DetalleOrden> getAll();

    @Query("SELECT * FROM detalleOrden WHERE orden_id = :ordenId")
    LiveData<List<DetalleOrden>> getAllByOrdenId(String ordenId);

    @Query("DELETE FROM detalleOrden")
    void deleteAll();

    @Query("DELETE FROM detalleOrden WHERE `temp` = :temp")
    void deleteAllByTemp(boolean temp);
}
