package com.martin.appcocina.db.views;

import androidx.room.DatabaseView;

import java.util.Objects;

@DatabaseView("SELECT o._id, o.total,c.nombres,c.apellido_paterno,c.apellido_materno FROM Orden o LEFT JOIN Cliente c ON o.cliente_id = c._id")
public class OrdenCliente {
    private String _id;
    private double total;
    private String nombres;
    private String apellido_paterno;
    private String apellido_materno;

    @Override
    public String
    toString() {
        return "OrdenCliente{" +
                "_id='" + _id + '\'' +
                ", total=" + total +
                ", nombres='" + nombres + '\'' +
                ", apellido_paterno='" + apellido_paterno + '\'' +
                ", apellido_materno='" + apellido_materno + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrdenCliente that = (OrdenCliente) o;

        if (Double.compare(that.total, total) != 0) return false;
        if (!Objects.equals(_id, that._id)) return false;
        if (!Objects.equals(nombres, that.nombres)) return false;
        if (!Objects.equals(apellido_paterno, that.apellido_paterno))
            return false;
        return Objects.equals(apellido_materno, that.apellido_materno);
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }
}
