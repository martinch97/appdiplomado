package com.martin.appcocina.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Cliente {
    @NonNull
    @PrimaryKey
    private String _id;
    private String imagen;
    private String nombres;
    private String apellido_paterno;
    private String apellido_materno;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cliente cliente = (Cliente) o;

        if (!Objects.equals(_id, cliente._id)) return false;
        if (!Objects.equals(imagen, cliente.imagen)) return false;
        if (!Objects.equals(nombres, cliente.nombres))
            return false;
        if (!Objects.equals(apellido_paterno, cliente.apellido_paterno))
            return false;
        return Objects.equals(apellido_materno, cliente.apellido_materno);
    }

    @NonNull
    public String get_id() {
        return _id;
    }

    public void set_id(@NonNull String _id) {
        this._id = _id;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }
}
