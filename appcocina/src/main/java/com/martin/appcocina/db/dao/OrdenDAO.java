package com.martin.appcocina.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martin.appcocina.db.entities.Orden;
import com.martin.appcocina.db.views.OrdenCliente;

import java.util.List;

@Dao
public interface OrdenDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Orden> ordenList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOne(Orden orden);

    @Query("SELECT * FROM orden")
    List<Orden> getAll();

    @Query("DELETE FROM orden")
    void deleteAll();

    @Query("SELECT * from ordencliente")
    LiveData<List<OrdenCliente>> getOrdenClienteAll();

    @Query("SELECT * from ordencliente WHERE _id = :ordenId")
    LiveData<OrdenCliente> getOrdenClienteById(String ordenId);
}
