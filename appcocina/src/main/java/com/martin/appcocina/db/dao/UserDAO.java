package com.martin.appcocina.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martin.appcocina.db.entities.User;

import java.util.List;

@Dao
public interface UserDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User user);

    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT _id FROM user LIMIT 1")
    String getUserId();

    @Query("DELETE FROM user")
    void deleteAll();
}
