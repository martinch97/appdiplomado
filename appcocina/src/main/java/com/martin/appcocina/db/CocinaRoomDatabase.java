package com.martin.appcocina.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.martin.appcocina.db.dao.ClienteDAO;
import com.martin.appcocina.db.dao.DetalleOrdenDAO;
import com.martin.appcocina.db.dao.OrdenDAO;
import com.martin.appcocina.db.dao.UserDAO;
import com.martin.appcocina.db.entities.Cliente;
import com.martin.appcocina.db.entities.DetalleOrden;
import com.martin.appcocina.db.entities.Orden;
import com.martin.appcocina.db.entities.User;
import com.martin.appcocina.db.views.OrdenCliente;

@Database(entities = {
        User.class,
        Cliente.class,
        DetalleOrden.class,
        Orden.class
}, views = {
        OrdenCliente.class
}, version = 4, exportSchema = false)
public abstract class CocinaRoomDatabase extends RoomDatabase {

    private static volatile CocinaRoomDatabase INSTANCE;

    public static CocinaRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CocinaRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room
                            .databaseBuilder(context.getApplicationContext(), CocinaRoomDatabase.class, "cocina_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract UserDAO userDAO();

    public abstract ClienteDAO clienteDAO();

    public abstract DetalleOrdenDAO detalleOrdenDAO();

    public abstract OrdenDAO ordenDAO();

}
