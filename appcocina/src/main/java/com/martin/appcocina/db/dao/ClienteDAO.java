package com.martin.appcocina.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martin.appcocina.db.entities.Cliente;

import java.util.List;

@Dao
public interface ClienteDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Cliente> clienteList);

    @Query("SELECT * FROM cliente")
    List<Cliente> getAll();

    @Query("DELETE FROM cliente")
    void deleteAll();
}
