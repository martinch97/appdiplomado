package com.martin.appcocina.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class DetalleOrden {
    @NonNull
    @PrimaryKey
    private String _id;
    private String orden_id;
    private String plato_id;
    private String nombre;
    private double precio;
    private int cantidad;
    private boolean temp;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DetalleOrden that = (DetalleOrden) o;

        if (Double.compare(that.precio, precio) != 0) return false;
        if (cantidad != that.cantidad) return false;
        if (temp != that.temp) return false;
        if (!_id.equals(that._id)) return false;
        if (!Objects.equals(orden_id, that.orden_id))
            return false;
        if (!Objects.equals(plato_id, that.plato_id))
            return false;
        return Objects.equals(nombre, that.nombre);
    }

    @Override
    public String toString() {
        return "DetalleOrden{" +
                "_id='" + _id + '\'' +
                ", orden_id='" + orden_id + '\'' +
                ", plato_id='" + plato_id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", precio=" + precio +
                ", cantidad=" + cantidad +
                ", temp=" + temp +
                '}';
    }

    @NonNull
    public String get_id() {
        return _id;
    }

    public void set_id(@NonNull String _id) {
        this._id = _id;
    }

    public String getOrden_id() {
        return orden_id;
    }

    public void setOrden_id(String orden_id) {
        this.orden_id = orden_id;
    }

    public String getPlato_id() {
        return plato_id;
    }

    public void setPlato_id(String plato_id) {
        this.plato_id = plato_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public boolean isTemp() {
        return temp;
    }

    public void setTemp(boolean temp) {
        this.temp = temp;
    }
}
