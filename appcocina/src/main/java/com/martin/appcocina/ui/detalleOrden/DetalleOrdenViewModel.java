package com.martin.appcocina.ui.detalleOrden;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.martin.appcocina.db.CocinaRoomDatabase;
import com.martin.appcocina.db.entities.DetalleOrden;
import com.martin.appcocina.db.views.OrdenCliente;
import com.martin.appcocina.util.AppExecutors;

import java.util.List;

public class DetalleOrdenViewModel extends AndroidViewModel {

    private CocinaRoomDatabase database;
    private LiveData<List<DetalleOrden>> detalleOrden;
    private LiveData<OrdenCliente> orden;
    private AppExecutors executors;
    private MutableLiveData<Boolean> loading;
    private MutableLiveData<String> mensaje;

    public DetalleOrdenViewModel(@NonNull Application application, String ordenId) {
        super(application);
        database = CocinaRoomDatabase.getDatabase(application);
        detalleOrden = database.detalleOrdenDAO().getAllByOrdenId(ordenId);
        orden = database.ordenDAO().getOrdenClienteById(ordenId);
        executors = new AppExecutors();
        loading = new MutableLiveData<>();
        mensaje = new MutableLiveData<>();
        loading.setValue(false);
    }

    public LiveData<OrdenCliente> getOrden() {
        return orden;
    }

    public MutableLiveData<String> getMensaje() {
        return mensaje;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public LiveData<List<DetalleOrden>> getDetalleOrden() {
        return detalleOrden;
    }
}
