package com.martin.appcocina.ui.detalleOrden;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.martin.appcocina.db.entities.DetalleOrden;

public class DetalleOrdenDiffCallback extends DiffUtil.ItemCallback<DetalleOrden> {
    @Override
    public boolean areItemsTheSame(@NonNull DetalleOrden oldItem, @NonNull DetalleOrden newItem) {
        return oldItem.get_id().equals(newItem.get_id());
    }

    @Override
    public boolean areContentsTheSame(@NonNull DetalleOrden oldItem, @NonNull DetalleOrden newItem) {
        return oldItem.equals(newItem);
    }
}
