package com.martin.appcocina.ui.ordenes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appcocina.R;
import com.martin.appcocina.db.views.OrdenCliente;

public class RvAdapter extends ListAdapter<OrdenCliente, RvAdapter.ViewHolder> {
    private OrdenClickListener ordenClickListener;

    public RvAdapter(@NonNull DiffUtil.ItemCallback<OrdenCliente> diffCallback, OrdenClickListener ordenClickListener) {
        super(diffCallback);
        this.ordenClickListener = ordenClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_orden, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrdenCliente orden = getItem(position);
        holder.tvName.setText(orden.getNombres());
        holder.itemView.setOnClickListener(view -> ordenClickListener.onClick(orden.get_id()));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
        }
    }
}
