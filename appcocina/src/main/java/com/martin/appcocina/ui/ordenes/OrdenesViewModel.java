package com.martin.appcocina.ui.ordenes;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.martin.appcocina.api.ApiManager;
import com.martin.appcocina.api.response.OrdenesResponse;
import com.martin.appcocina.api.socket.OnNuevaOrdenDeCompra;
import com.martin.appcocina.api.socket.ResponseLoginUsuario;
import com.martin.appcocina.db.CocinaRoomDatabase;
import com.martin.appcocina.db.entities.Cliente;
import com.martin.appcocina.db.entities.DetalleOrden;
import com.martin.appcocina.db.entities.Orden;
import com.martin.appcocina.db.views.OrdenCliente;
import com.martin.appcocina.util.AppExecutors;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdenesViewModel extends AndroidViewModel {

    private CocinaRoomDatabase database;
    private LiveData<List<OrdenCliente>> ordenes;
    private AppExecutors executors;
    private MutableLiveData<Boolean> loading;
    private MutableLiveData<String> mensaje;
    private Socket socket;
    private String nuevaOrdenCompra = "nuevaOrdenDeCompra";
    private Gson gson;
    private Emitter.Listener onConnect = args -> {
        mensaje.postValue("Socket conectado");
        loginSocket();
    };
    private Emitter.Listener onNuevaOrdenDeCompra = args -> {
        loading.postValue(true);
        OnNuevaOrdenDeCompra ordenSocket = gson.fromJson(args[0].toString(), OnNuevaOrdenDeCompra.class);
        List<DetalleOrden> detalleOrdenList = new ArrayList<>();
        for (OnNuevaOrdenDeCompra.OnOrden onOrden : ordenSocket.getOrden()) {
            DetalleOrden detalleOrden = new DetalleOrden();
            detalleOrden.set_id(UUID.randomUUID().toString());
            detalleOrden.setOrden_id(ordenSocket.get_id());
            detalleOrden.setPlato_id(onOrden.getPlato_id());
            detalleOrden.setNombre(onOrden.getNombre());
            detalleOrden.setPrecio(onOrden.getPrecio());
            detalleOrden.setCantidad(onOrden.getCantidad());
            detalleOrden.setTemp(true);
            detalleOrdenList.add(detalleOrden);
        }
        Orden orden = new Orden();
        orden.set_id(ordenSocket.get_id());
        orden.setFechaRegistro(ordenSocket.getFechaRegistro());
        orden.setCliente_id(ordenSocket.getCliente_id());
        orden.setTotal(ordenSocket.getTotal());
        orden.set__v(0);
        database.ordenDAO().insertOne(orden);
        database.detalleOrdenDAO().insert(detalleOrdenList);
        loading.postValue(false);
        Log.d("DetalleOrdenViewModel", ": " + ordenSocket.toString());
    };


    public OrdenesViewModel(@NonNull Application application) {
        super(application);
        database = CocinaRoomDatabase.getDatabase(application);
        ordenes = database.ordenDAO().getOrdenClienteAll();
        executors = new AppExecutors();
        loading = new MutableLiveData<>();
        mensaje = new MutableLiveData<>();
        gson = new Gson();
        loading.setValue(false);
        getOrdenesFromService();
        try {
            socket = IO.socket("https://diplomado-restaurant-backend.herokuapp.com");
            socket.connect();
            socket.on(Socket.EVENT_CONNECT, onConnect);
            socket.on(nuevaOrdenCompra, onNuevaOrdenDeCompra);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void loginSocket() {
        if (socket.connected()) {
            loading.postValue(true);
            executors.diskIO().execute(() -> {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("user_id", database.userDAO().getUserId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                socket.emit("loginUsuario", jsonObject, (Ack) args -> {
                    ResponseLoginUsuario responseLoginUsuario = gson.fromJson(args[0].toString(), ResponseLoginUsuario.class);
                    Log.d("DetalleOrdenViewModel", "loginUsuario: " + responseLoginUsuario);
                    mensaje.postValue(responseLoginUsuario.getMessage());
                    loading.postValue(false);
                });
            });

        }
    }

    public MutableLiveData<String> getMensaje() {
        return mensaje;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public LiveData<List<OrdenCliente>> getOrdenes() {
        return ordenes;
    }

    private void getOrdenesFromService() {
        loading.setValue(true);
        mensaje.setValue("Estoy trayendo data");
        ApiManager.getApiManagerInstance().getOrdenes().enqueue(new Callback<OrdenesResponse>() {
            @Override
            public void onResponse(Call<OrdenesResponse> call, Response<OrdenesResponse> response) {
                if (response.isSuccessful()) {
                    executors.diskIO().execute(() -> {
                        database.detalleOrdenDAO().deleteAllByTemp(true);
                        List<Cliente> clienteList = new ArrayList<>();
                        List<Orden> ordenList = new ArrayList<>();
                        List<DetalleOrden> detalleOrdenList = new ArrayList<>();
                        for (OrdenesResponse.OrdenRetrofit data : response.body().getData()) {
                            ordenList.add(getOrdenFromRetrofit(data));
                            clienteList.add(getClienteFromRetrofit(data.getCliente_id()));
                            for (OrdenesResponse.DetalleOrdenRetrofit detalleOrdenRetrofit : data.getDetalle()) {
                                detalleOrdenList.add(getDetalleOrdenFromRetrofit(data, detalleOrdenRetrofit));
                            }
                        }
                        database.clienteDAO().insert(clienteList);
                        database.ordenDAO().insert(ordenList);
                        database.detalleOrdenDAO().insert(detalleOrdenList);
                        loading.postValue(false);
                        mensaje.postValue("Terminé de traer data");
                    });
                } else {
                    loading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<OrdenesResponse> call, Throwable t) {
                loading.setValue(false);
                Log.w("TAG", "onFailure: ", t);
            }
        });
    }

    private DetalleOrden getDetalleOrdenFromRetrofit(OrdenesResponse.OrdenRetrofit data, OrdenesResponse.DetalleOrdenRetrofit detalleOrdenRetrofit) {
        DetalleOrden detalleOrden = new DetalleOrden();
        detalleOrden.set_id(detalleOrdenRetrofit.get_id());
        detalleOrden.setOrden_id(data.get_id());
        detalleOrden.setPlato_id(detalleOrdenRetrofit.getPlato_id());
        detalleOrden.setNombre(detalleOrdenRetrofit.getNombre());
        detalleOrden.setPrecio(detalleOrdenRetrofit.getPrecio());
        detalleOrden.setCantidad(detalleOrdenRetrofit.getCantidad());
        detalleOrden.setTemp(false);
        return detalleOrden;
    }

    private Cliente getClienteFromRetrofit(OrdenesResponse.ClienteRetrofit data) {
        Cliente cliente = new Cliente();
        cliente.set_id(data.get_id());
        cliente.setImagen(data.getImagen());
        cliente.setNombres(data.getNombres());
        cliente.setApellido_paterno(data.getApellido_paterno());
        cliente.setApellido_materno(data.getApellido_materno());
        return cliente;
    }

    private Orden getOrdenFromRetrofit(OrdenesResponse.OrdenRetrofit data) {
        Orden orden = new Orden();
        orden.set_id(data.get_id());
        orden.setFechaRegistro(data.getFechaRegistro());
        orden.setCliente_id(data.getCliente_id().get_id());
        orden.setTotal(data.getTotal());
        orden.set__v(data.get__v());
        return orden;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        socket.off(Socket.EVENT_CONNECT);
        socket.off(nuevaOrdenCompra);
        socket.disconnect();
    }
}
