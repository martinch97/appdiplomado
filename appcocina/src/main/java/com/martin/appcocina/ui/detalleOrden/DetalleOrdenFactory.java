package com.martin.appcocina.ui.detalleOrden;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class DetalleOrdenFactory implements ViewModelProvider.Factory {
    private Application application;
    private String ordenId;

    public DetalleOrdenFactory(Application application, String ordenId) {
        this.application = application;
        this.ordenId = ordenId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(DetalleOrdenViewModel.class)) {
            return (T) new DetalleOrdenViewModel(application, ordenId);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
