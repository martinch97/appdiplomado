package com.martin.appcocina.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;

import com.martin.appcocina.R;
import com.martin.appcocina.ui.base.BaseActivity;
import com.martin.appcocina.ui.ordenes.OrdenesActivity;

public class LoginActivity extends BaseActivity {
    private EditText etEmail;
    private EditText etPassword;
    private SharedPreferences preferences;
    private LoginViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferences = getSharedPreferences("CocinaApp", Context.MODE_PRIVATE);
        if (preferences.getBoolean("logged", false)) {
            startActivity(new Intent(this, OrdenesActivity.class));
            finish();
        } else {
            etEmail = findViewById(R.id.etEmail);
            etPassword = findViewById(R.id.etContrasena);
            etPassword.setOnEditorActionListener((textView, i, keyEvent) -> {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    viewModel.login(etEmail.getText().toString(), etPassword.getText().toString());
                    return true;
                }
                return false;
            });
            viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
            viewModel.getMensaje().observe(this, mensaje -> Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show());
            viewModel.getLoading().observe(this, loading -> {
                if (loading) {
                    showProgressDialog();
                } else {
                    hideProgressDialog();
                }
            });
            viewModel.getGoToOrdenes().observe(this, goToOrdenes -> {
                if (goToOrdenes) {
                    startActivity(new Intent(LoginActivity.this, OrdenesActivity.class));
                    finish();
                }
            });
        }
    }
}
