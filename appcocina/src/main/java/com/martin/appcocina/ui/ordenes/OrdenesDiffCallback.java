package com.martin.appcocina.ui.ordenes;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.martin.appcocina.db.views.OrdenCliente;

public class OrdenesDiffCallback extends DiffUtil.ItemCallback<OrdenCliente> {
    @Override
    public boolean areItemsTheSame(@NonNull OrdenCliente oldItem, @NonNull OrdenCliente newItem) {
        return oldItem.get_id().equals(newItem.get_id());
    }

    @Override
    public boolean areContentsTheSame(@NonNull OrdenCliente oldItem, @NonNull OrdenCliente newItem) {
        return oldItem.equals(newItem);
    }
}
