package com.martin.appcocina.ui.login;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.martin.appcocina.api.ApiManager;
import com.martin.appcocina.api.request.LoginRequest;
import com.martin.appcocina.api.response.LoginResponse;
import com.martin.appcocina.db.CocinaRoomDatabase;
import com.martin.appcocina.util.AppExecutors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends AndroidViewModel {
    private CocinaRoomDatabase database;
    private AppExecutors executors;
    private MutableLiveData<Boolean> loading;
    private MutableLiveData<Boolean> goToOrdenes;
    private MutableLiveData<String> mensaje;
    private SharedPreferences preferences;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        database = CocinaRoomDatabase.getDatabase(application);
        preferences = application.getSharedPreferences("CocinaApp", Context.MODE_PRIVATE);
        executors = new AppExecutors();
        loading = new MutableLiveData<>();
        goToOrdenes = new MutableLiveData<>();
        mensaje = new MutableLiveData<>();
        loading.setValue(false);
        goToOrdenes.setValue(false);
    }

    void login(String email, String password) {
        loading.setValue(true);
        LoginRequest request = new LoginRequest();
        request.setEmail(email);
        request.setPassword(password);
        ApiManager.getApiManagerInstance().login(request).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    executors.diskIO().execute(() -> {
                        database.userDAO().insert(response.body().getData());
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("logged", true);
                        editor.commit();
                        mensaje.postValue(response.body().getData().toString());
                        loading.postValue(false);
                        goToOrdenes.postValue(true);
                    });
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                mensaje.setValue(t.getMessage());
            }
        });

    }

    public MutableLiveData<Boolean> getGoToOrdenes() {
        return goToOrdenes;
    }

    public MutableLiveData<String> getMensaje() {
        return mensaje;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

}
