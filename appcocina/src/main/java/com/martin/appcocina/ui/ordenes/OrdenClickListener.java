package com.martin.appcocina.ui.ordenes;

public interface OrdenClickListener {
    void onClick(String ordenId);
}
