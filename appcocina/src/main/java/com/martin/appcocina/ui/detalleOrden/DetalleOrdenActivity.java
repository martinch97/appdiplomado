package com.martin.appcocina.ui.detalleOrden;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appcocina.R;
import com.martin.appcocina.ui.base.BaseActivity;

public class DetalleOrdenActivity extends BaseActivity {
    private DetalleOrdenViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_orden);
        RvAdapter adapter = new RvAdapter(new DetalleOrdenDiffCallback());
        viewModel = ViewModelProviders.of(this, new DetalleOrdenFactory(getApplication(), getIntent().getStringExtra("ordenId"))).get(DetalleOrdenViewModel.class);
        TextView tvCliente = findViewById(R.id.tvCliente);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        viewModel.getOrden().observe(this, ordenCliente -> tvCliente.setText("Cliente: " + ordenCliente.getNombres()));
        viewModel.getLoading().observe(this, loading -> {
            if (loading) {
                showProgressDialog();
            } else {
                hideProgressDialog();
            }
        });
        viewModel.getDetalleOrden().observe(this, adapter::submitList);
        viewModel.getMensaje().observe(this, mensaje -> Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show());
    }
}
