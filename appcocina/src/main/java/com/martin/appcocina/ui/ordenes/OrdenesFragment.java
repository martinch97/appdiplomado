package com.martin.appcocina.ui.ordenes;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appcocina.R;
import com.martin.appcocina.ui.base.BaseFragment;
import com.martin.appcocina.ui.detalleOrden.DetalleOrdenActivity;


/**
 * A placeholder fragment containing a simple view.
 */
public class OrdenesFragment extends BaseFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private int sectionNumber;
    private OrdenesViewModel viewModel;

    public static OrdenesFragment newInstance(int sectionNumber) {
        OrdenesFragment fragment = new OrdenesFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ordenes, container, false);
        RvAdapter adapter = new RvAdapter(new OrdenesDiffCallback(), ordenId -> {
            Intent intent = new Intent(getActivity(), DetalleOrdenActivity.class);
            intent.putExtra("ordenId", ordenId);
            startActivity(intent);
        });
        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        viewModel = ViewModelProviders.of(this).get(OrdenesViewModel.class);
        viewModel.getLoading().observe(this, loading -> {
            if (loading) {
                showProgressDialog();
            } else {
                hideProgressDialog();
            }
        });
        viewModel.getOrdenes().observe(this, adapter::submitList);
//        viewModel.getOrdenes().observe(this, ordenClientes -> {
//            adapter.submitList(ordenClientes);
//        });
        viewModel.getMensaje().observe(this, mensaje -> Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show());
        return root;
    }
}