package com.martin.appcocina.ui.detalleOrden;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appcocina.R;
import com.martin.appcocina.db.entities.DetalleOrden;

public class RvAdapter extends ListAdapter<DetalleOrden, RvAdapter.ViewHolder> {

    RvAdapter(@NonNull DiffUtil.ItemCallback<DetalleOrden> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detalle_orden, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DetalleOrden detalleOrden = getItem(position);
        holder.tvCantidad.setText(String.valueOf(detalleOrden.getCantidad()));
        holder.tvPlato.setText(detalleOrden.getNombre());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCantidad;
        private TextView tvPlato;
        private CheckBox cbCocinado;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCantidad = itemView.findViewById(R.id.tvCantidad);
            tvPlato = itemView.findViewById(R.id.tvPlato);
            cbCocinado = itemView.findViewById(R.id.cbCocinado);
        }
    }
}
