package com.martin.appcocina.api.response;

import java.util.List;

public class OrdenesResponse {
    private String message;
    private List<OrdenRetrofit> data;

    public String getMessage() {
        return message;
    }

    public List<OrdenRetrofit> getData() {
        return data;
    }

    public class OrdenRetrofit {
        private String fechaRegistro;
        private String _id;
        private ClienteRetrofit cliente_id;
        private double total;
        private List<DetalleOrdenRetrofit> detalle;
        private double __v;

        public String getFechaRegistro() {
            return fechaRegistro;
        }

        public String get_id() {
            return _id;
        }

        public ClienteRetrofit getCliente_id() {
            return cliente_id;
        }

        public double getTotal() {
            return total;
        }

        public List<DetalleOrdenRetrofit> getDetalle() {
            return detalle;
        }

        public double get__v() {
            return __v;
        }
    }

    public class ClienteRetrofit {
        private String imagen;
        private String _id;
        private String nombres;
        private String apellido_paterno;
        private String apellido_materno;

        public String getImagen() {
            return imagen;
        }

        public String get_id() {
            return _id;
        }

        public String getNombres() {
            return nombres;
        }

        public String getApellido_paterno() {
            return apellido_paterno;
        }

        public String getApellido_materno() {
            return apellido_materno;
        }
    }

    public class DetalleOrdenRetrofit {
        private String _id;
        private String plato_id;
        private String nombre;
        private double precio;
        private int cantidad;

        public String get_id() {
            return _id;
        }

        public String getPlato_id() {
            return plato_id;
        }

        public String getNombre() {
            return nombre;
        }

        public double getPrecio() {
            return precio;
        }

        public int getCantidad() {
            return cantidad;
        }
    }
}
