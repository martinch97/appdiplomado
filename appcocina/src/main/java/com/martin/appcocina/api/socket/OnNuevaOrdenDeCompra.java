package com.martin.appcocina.api.socket;

import java.util.List;

public class OnNuevaOrdenDeCompra {
    private String _id;
    private String cliente_id;
    private String nombreCliente;
    private double total;
    private String fechaRegistro;
    private List<OnOrden> orden;

    @Override
    public String toString() {
        return "OnNuevaOrdenDeCompra{" +
                "_id='" + _id + '\'' +
                ", cliente_id='" + cliente_id + '\'' +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", total=" + total +
                ", fechaRegistro='" + fechaRegistro + '\'' +
                ", orden=" + orden +
                '}';
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(String cliente_id) {
        this.cliente_id = cliente_id;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public List<OnOrden> getOrden() {
        return orden;
    }

    public void setOrden(List<OnOrden> orden) {
        this.orden = orden;
    }

    public class OnOrden {
        private String plato_id;
        private String nombre;
        private double precio;
        private int cantidad;

        @Override
        public String toString() {
            return "OnOrden{" +
                    "plato_id='" + plato_id + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", precio=" + precio +
                    ", cantidad=" + cantidad +
                    '}';
        }

        public String getPlato_id() {
            return plato_id;
        }

        public void setPlato_id(String plato_id) {
            this.plato_id = plato_id;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public double getPrecio() {
            return precio;
        }

        public void setPrecio(double precio) {
            this.precio = precio;
        }

        public int getCantidad() {
            return cantidad;
        }

        public void setCantidad(int cantidad) {
            this.cantidad = cantidad;
        }
    }
}
