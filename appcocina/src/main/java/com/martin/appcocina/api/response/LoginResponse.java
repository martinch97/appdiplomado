package com.martin.appcocina.api.response;

import com.martin.appcocina.db.entities.User;

public class LoginResponse {
    private String message;
    private User data;

    public String getMessage() {
        return message;
    }

    public User getData() {
        return data;
    }
}
