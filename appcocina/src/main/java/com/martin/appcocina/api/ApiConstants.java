package com.martin.appcocina.api;

class ApiConstants {
    static final String BASE_URL = "https://diplomado-restaurant-backend.herokuapp.com/";
    static final int TIMEOUT = 60; // secs

    static final String LOGIN = "auth/usuarios-login";
    static final String GET_ORDENES = "ordenes-de-compra";
}
