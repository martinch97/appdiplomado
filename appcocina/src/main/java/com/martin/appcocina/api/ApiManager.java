package com.martin.appcocina.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {
    private static ApiService mApiService;

    public static ApiService getApiManagerInstance() {
        if (mApiService == null) {
            mApiService = provideApiManager();
        }
        return mApiService;
    }

    private static ApiService provideApiManager() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY))
                .readTimeout(ApiConstants.TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(ApiConstants.TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(ApiConstants.TIMEOUT, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        retrofit.create(ApiService.class);
        return retrofit.create(ApiService.class);
    }
}
