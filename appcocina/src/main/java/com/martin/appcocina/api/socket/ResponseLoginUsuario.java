package com.martin.appcocina.api.socket;

public class ResponseLoginUsuario {
    private boolean success;
    private String message;

    @Override
    public String toString() {
        return "ResponseOrdenCompra{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
