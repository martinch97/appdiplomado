package com.martin.appdiplomado.ui.orden;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appdiplomado.R;
import com.martin.appdiplomado.db.entities.Orden;

public class RvAdapter extends ListAdapter<Orden, RvAdapter.ViewHolder> {

    RvAdapter(@NonNull DiffUtil.ItemCallback<Orden> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public RvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_orden, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RvAdapter.ViewHolder holder, int position) {
        Orden orden = getItem(position);
        holder.tvCantidad.setText(String.valueOf(orden.getCantidad()));
        holder.tvPlato.setText(orden.getNombre());
        holder.tvPrecio.setText(String.valueOf(orden.getPrecio()));
        holder.tvTotal.setText(String.valueOf(orden.getPrecio() * orden.getCantidad()));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCantidad;
        private TextView tvPlato;
        private TextView tvPrecio;
        private TextView tvTotal;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCantidad = itemView.findViewById(R.id.tvCantidad);
            tvPlato = itemView.findViewById(R.id.tvPlato);
            tvPrecio = itemView.findViewById(R.id.tvPrecio);
            tvTotal = itemView.findViewById(R.id.tvTotal);
        }
    }
}
