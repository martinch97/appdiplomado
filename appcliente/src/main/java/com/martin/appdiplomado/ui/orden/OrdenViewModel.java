package com.martin.appdiplomado.ui.orden;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.martin.appdiplomado.api.socket.ResponseOrdenCompra;
import com.martin.appdiplomado.db.ClienteRoomDatabase;
import com.martin.appdiplomado.db.entities.Orden;
import com.martin.appdiplomado.util.AppExecutors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.List;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class OrdenViewModel extends AndroidViewModel {
    private ClienteRoomDatabase database;
    private LiveData<List<Orden>> ordenes;
    private AppExecutors executors;
    private MutableLiveData<Boolean> loading;
    private MutableLiveData<String> mensaje;
    private Socket socket;
    private Gson gson;
    private Emitter.Listener onConnect = args -> {
        Log.d("OrdenViewModel", "onConnect: ");
        mensaje.postValue("Socket conectado");
    };

    public OrdenViewModel(@NonNull Application application) {
        super(application);
        database = ClienteRoomDatabase.getDatabase(application);
        ordenes = database.ordenDAO().getAll();
        executors = new AppExecutors();
        loading = new MutableLiveData<>();
        mensaje = new MutableLiveData<>();
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        loading.setValue(false);
        try {
//            socket = IO.socket("http://192.168.1.39:3000");
            socket = IO.socket("https://diplomado-restaurant-backend.herokuapp.com");
            socket.connect();
            socket.on(Socket.EVENT_CONNECT, onConnect);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    void sendOrder() {
        executors.diskIO().execute(() -> {
            String clienteId = database.clienteDAO().getClienteId();
            if (clienteId != null) {
                if (socket.connected()) {
                    loading.postValue(true);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("cliente_id", clienteId);
                        JSONArray jsonArray = new JSONArray();
                        for (Orden orden : ordenes.getValue()) {
                            JSONObject objItem = new JSONObject();
                            objItem.put("plato_id", orden.get_id());
                            objItem.put("nombre", orden.getNombre());
                            objItem.put("precio", orden.getPrecio());
                            objItem.put("cantidad", orden.getCantidad());
                            jsonArray.put(objItem);
                        }
                        jsonObject.put("orden", jsonArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    socket.emit("registrarOrdenDeCompra", jsonObject, (Ack) args -> {
                        ResponseOrdenCompra responseOrdenCompra = gson.fromJson(args[0].toString(), ResponseOrdenCompra.class);
                        Log.d("OrdenViewModel", "registrarOrdenDeCompra: " + responseOrdenCompra);
                        if (responseOrdenCompra.isSuccess()) {
                            database.ordenDAO().deleteAll();
                        }
                        mensaje.postValue(responseOrdenCompra.getMessage());
                        loading.postValue(false);
                    });
                } else {
                    mensaje.postValue("El socket no está conectado");
                }
            } else {
                mensaje.postValue("Debes estar logeado para poder crear una orden");
            }
        });
    }

    public LiveData<List<Orden>> getOrdenes() {
        return ordenes;
    }

    public MutableLiveData<String> getMensaje() {
        return mensaje;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        socket.off(Socket.EVENT_CONNECT);
        socket.disconnect();
    }
}
