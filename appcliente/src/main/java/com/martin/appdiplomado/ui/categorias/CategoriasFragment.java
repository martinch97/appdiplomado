package com.martin.appdiplomado.ui.categorias;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appdiplomado.R;
import com.martin.appdiplomado.ui.base.BaseFragment;

public class CategoriasFragment extends BaseFragment {
    private CategoriasViewModel viewModel;
    private NavController navController;

    public CategoriasFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_categorias, container, false);
        RvAdapter adapter = new RvAdapter(new CategoriasDiffCallback(), categoriaId -> viewModel.onCategoriaClicked(categoriaId));
        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        navController = NavHostFragment.findNavController(this);
        viewModel = ViewModelProviders.of(this).get(CategoriasViewModel.class);
        viewModel.getLoading().observe(this, loading -> {
            if (loading) {
                showProgressDialog();
            } else {
                hideProgressDialog();
            }
        });
        viewModel.getCategorias().observe(this, adapter::submitList);
        viewModel.getMensaje().observe(this, mensaje -> Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show());
        viewModel.getNavigateToPlatos().observe(this, categoriaId -> {
            if (categoriaId != null) {
                navController.navigate(CategoriasFragmentDirections.actionCategoriasFragmentToPlatosFragment(categoriaId));
                viewModel.doneNavigating();
            }
        });
        return root;
    }
}
