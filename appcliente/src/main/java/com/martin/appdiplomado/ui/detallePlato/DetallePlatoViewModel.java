package com.martin.appdiplomado.ui.detallePlato;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.martin.appdiplomado.db.ClienteRoomDatabase;
import com.martin.appdiplomado.db.entities.Plato;
import com.martin.appdiplomado.util.AppExecutors;

public class DetallePlatoViewModel extends AndroidViewModel {
    private ClienteRoomDatabase database;
    private String platoId;
    private LiveData<Plato> plato;
    private AppExecutors executors;
    private MutableLiveData<Boolean> loading;
    private MutableLiveData<String> mensaje;

    public DetallePlatoViewModel(@NonNull Application application, String platoId) {
        super(application);
        database = ClienteRoomDatabase.getDatabase(application);
        this.platoId = platoId;
        plato = database.platoDAO().getPlatoById(platoId);
        executors = new AppExecutors();
        loading = new MutableLiveData<>();
        mensaje = new MutableLiveData<>();
        loading.setValue(false);
    }

    public LiveData<Plato> getPlato() {
        return plato;
    }

    public MutableLiveData<String> getMensaje() {
        return mensaje;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

}
