package com.martin.appdiplomado.ui.platos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appdiplomado.R;
import com.martin.appdiplomado.db.entities.Plato;
import com.martin.appdiplomado.util.GlideRequests;

import java.text.DecimalFormat;

public class RvAdapter extends ListAdapter<Plato, RvAdapter.ViewHolder> {

    private PlatoClickListener platoClickListener;
    private GlideRequests glideRequests;
    private DecimalFormat decimalFormat;

    RvAdapter(@NonNull DiffUtil.ItemCallback<Plato> diffCallback, PlatoClickListener platoClickListener, GlideRequests glideRequests) {
        super(diffCallback);
        this.platoClickListener = platoClickListener;
        this.glideRequests = glideRequests;
        decimalFormat = new DecimalFormat("###,###.00");
    }

    @NonNull
    @Override
    public RvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plato, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RvAdapter.ViewHolder holder, int position) {
        Plato plato = getItem(position);
        holder.tvPlato.setText(plato.getNombre());
        holder.itemView.setOnClickListener(view -> platoClickListener.onClick(plato.get_id()));
        glideRequests.load("https://diplomado-restaurant-backend.herokuapp.com/images/platos/" + plato.getImagen()).into(holder.ivPlato);
        holder.tvPrecio.setText("S/." + decimalFormat.format(plato.getPrecio()));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPlato;
        private ImageView ivPlato;
        private TextView tvPrecio;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPlato = itemView.findViewById(R.id.tvPlato);
            ivPlato = itemView.findViewById(R.id.ivPlato);
            tvPrecio = itemView.findViewById(R.id.tvPrecio);
        }
    }
}
