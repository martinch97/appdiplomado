package com.martin.appdiplomado.ui.orden;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appdiplomado.R;
import com.martin.appdiplomado.db.entities.Orden;
import com.martin.appdiplomado.ui.base.BaseFragment;

public class OrdenFragment extends BaseFragment {
    private OrdenViewModel viewModel;
    private NavController navController;

    public OrdenFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_orden, container, false);
        RvAdapter adapter = new RvAdapter(new OrdenDiffCallback());
        TextView tvTotalValue = root.findViewById(R.id.tvTotalValue);
        TextView tvConfirmar = root.findViewById(R.id.tvConfirmar);
        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        navController = NavHostFragment.findNavController(this);
        viewModel = ViewModelProviders.of(this).get(OrdenViewModel.class);
        viewModel.getLoading().observe(this, loading -> {
            if (loading) {
                showProgressDialog();
            } else {
                hideProgressDialog();
            }
        });
        viewModel.getOrdenes().observe(this, ordenes -> {
            adapter.submitList(ordenes);
            double totalValue = 0;
            for (Orden orden : ordenes) {
                totalValue += (orden.getCantidad() * orden.getPrecio());
            }
            tvTotalValue.setText(String.valueOf(totalValue));
        });
        viewModel.getMensaje().observe(this, mensaje -> Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show());
        tvConfirmar.setOnClickListener(view -> viewModel.sendOrder());
        return root;
    }

}
