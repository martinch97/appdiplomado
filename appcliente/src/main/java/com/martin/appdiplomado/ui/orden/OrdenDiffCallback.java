package com.martin.appdiplomado.ui.orden;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.martin.appdiplomado.db.entities.Orden;

public class OrdenDiffCallback extends DiffUtil.ItemCallback<Orden> {
    @Override
    public boolean areItemsTheSame(@NonNull Orden oldItem, @NonNull Orden newItem) {
        return oldItem.get_id() == newItem.get_id();
    }

    @Override
    public boolean areContentsTheSame(@NonNull Orden oldItem, @NonNull Orden newItem) {
        return oldItem.equals(newItem);
    }
}
