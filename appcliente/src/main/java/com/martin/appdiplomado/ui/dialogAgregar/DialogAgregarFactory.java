package com.martin.appdiplomado.ui.dialogAgregar;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class DialogAgregarFactory implements ViewModelProvider.Factory {
    private Application application;
    private String platoId;

    public DialogAgregarFactory(Application application, String platoId) {
        this.application = application;
        this.platoId = platoId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(DialogAgregarViewModel.class)) {
            return (T) new DialogAgregarViewModel(application, platoId);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
