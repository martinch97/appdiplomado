package com.martin.appdiplomado.ui.platos;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class PlatosFactory implements ViewModelProvider.Factory {
    private Application application;
    private String categoriaId;

    public PlatosFactory(Application application, String categoriaId) {
        this.application = application;
        this.categoriaId = categoriaId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(PlatosViewModel.class)) {
            return (T) new PlatosViewModel(application, categoriaId);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
