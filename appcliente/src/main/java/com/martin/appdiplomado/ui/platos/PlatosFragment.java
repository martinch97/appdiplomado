package com.martin.appdiplomado.ui.platos;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.martin.appdiplomado.R;
import com.martin.appdiplomado.ui.base.BaseFragment;
import com.martin.appdiplomado.util.GlideApp;

public class PlatosFragment extends BaseFragment {
    private PlatosViewModel viewModel;
    private NavController navController;

    public PlatosFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_platos, container, false);
        RvAdapter adapter = new RvAdapter(new PlatosDiffCallback(), platoId -> viewModel.onPlatoClicked(platoId), GlideApp.with(this));
//        RvAdapter adapter = new RvAdapter(new PlatosDiffCallback(), platoId -> new PlatoClickListener() {
//            @Override
//            public void onClick(String platoId) {
//                viewModel.onPlatoClicked(platoId);
//            }
//        }, GlideApp.with(this));
        ImageView ivCliente = root.findViewById(R.id.ivCliente);
        TextInputLayout ilEmail = root.findViewById(R.id.ilEmail);
        TextInputLayout ilPassword = root.findViewById(R.id.ilPassword);
        TextInputEditText etEmail = root.findViewById(R.id.etEmail);
        TextInputEditText etPassword = root.findViewById(R.id.etPassword);
        Button btnLogin = root.findViewById(R.id.btnLogin);
        TextView tvCliente = root.findViewById(R.id.tvCliente);
        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        navController = NavHostFragment.findNavController(this);
        viewModel = ViewModelProviders.of(this, new PlatosFactory(getActivity().getApplication(), PlatosFragmentArgs.fromBundle(getArguments()).getCategoriaId())).get(PlatosViewModel.class);
        viewModel.getLoading().observe(this, loading -> {
            if (loading) {
                showProgressDialog();
            } else {
                hideProgressDialog();
            }
        });
        viewModel.getPlatos().observe(this, adapter::submitList);
//        viewModel.getPlatos().observe(this, new Observer<List<Plato>>() {
//            @Override
//            public void onChanged(List<Plato> platoes) {
//                adapter.submitList(platoes);
//            }
//        });
        viewModel.getMensaje().observe(this, mensaje -> Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show());
        viewModel.getNavigateToDetallePlato().observe(this, platoId -> {
            if (platoId != null) {
                navController.navigate(PlatosFragmentDirections.actionPlatosFragmentToDetallePlatoFragment(platoId));
                viewModel.doneNavigating();
            }
        });
        viewModel.getCliente().observe(this, cliente -> {
            if (cliente != null) {
                ilEmail.setVisibility(View.GONE);
                ilPassword.setVisibility(View.GONE);
                btnLogin.setVisibility(View.GONE);
                tvCliente.setVisibility(View.VISIBLE);
                tvCliente.setText(cliente.getNombres());
                GlideApp.with(PlatosFragment.this).load("https://diplomado-restaurant-backend.herokuapp.com/images/clientes/" + cliente.getImagen()).into(ivCliente);
            } else {
                ilEmail.setVisibility(View.VISIBLE);
                ilPassword.setVisibility(View.VISIBLE);
                btnLogin.setVisibility(View.VISIBLE);
                tvCliente.setVisibility(View.GONE);
            }
        });
        btnLogin.setOnClickListener(view -> viewModel.login(etEmail.getText().toString(), etPassword.getText().toString()));
        return root;
    }

}
