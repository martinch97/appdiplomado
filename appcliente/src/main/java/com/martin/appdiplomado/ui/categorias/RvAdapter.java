package com.martin.appdiplomado.ui.categorias;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.martin.appdiplomado.R;
import com.martin.appdiplomado.db.entities.Categoria;

public class RvAdapter extends ListAdapter<Categoria, RvAdapter.ViewHolder> {

    private CategoriaClickListener categoriaClickListener;

    public RvAdapter(@NonNull DiffUtil.ItemCallback<Categoria> diffCallback, CategoriaClickListener categoriaClickListener) {
        super(diffCallback);
        this.categoriaClickListener = categoriaClickListener;
    }

    @NonNull
    @Override
    public RvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_categoria, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RvAdapter.ViewHolder holder, int position) {
        Categoria categoria = getItem(position);
        holder.tvCategoria.setText(categoria.getNombre());
        holder.itemView.setOnClickListener(view -> categoriaClickListener.onClick(categoria.get_id()));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCategoria;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoria = itemView.findViewById(R.id.tvCategoria);
        }
    }
}
