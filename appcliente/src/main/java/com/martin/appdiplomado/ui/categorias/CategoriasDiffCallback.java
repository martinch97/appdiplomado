package com.martin.appdiplomado.ui.categorias;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.martin.appdiplomado.db.entities.Categoria;

public class CategoriasDiffCallback extends DiffUtil.ItemCallback<Categoria> {
    @Override
    public boolean areItemsTheSame(@NonNull Categoria oldItem, @NonNull Categoria newItem) {
        return oldItem.get_id().equals(newItem.get_id());
    }

    @Override
    public boolean areContentsTheSame(@NonNull Categoria oldItem, @NonNull Categoria newItem) {
        return oldItem.equals(newItem);
    }
}
