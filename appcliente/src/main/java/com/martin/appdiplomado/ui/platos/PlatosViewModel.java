package com.martin.appdiplomado.ui.platos;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.martin.appdiplomado.api.ApiManager;
import com.martin.appdiplomado.api.request.LoginRequest;
import com.martin.appdiplomado.api.response.LoginResponse;
import com.martin.appdiplomado.api.response.PlatoCategoriaResponse;
import com.martin.appdiplomado.db.ClienteRoomDatabase;
import com.martin.appdiplomado.db.entities.Cliente;
import com.martin.appdiplomado.db.entities.Plato;
import com.martin.appdiplomado.util.AppExecutors;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class PlatosViewModel extends AndroidViewModel {
    private ClienteRoomDatabase database;
    private String categoriaId;
    private LiveData<List<Plato>> platos;
    private LiveData<Cliente> cliente;
    private AppExecutors executors;
    private MutableLiveData<Boolean> loading;
    private MutableLiveData<String> mensaje;
    private MutableLiveData<String> navigateToDetallePlato;

    PlatosViewModel(@NonNull Application application, String categoriaId) {
        super(application);
        this.categoriaId = categoriaId;
        database = ClienteRoomDatabase.getDatabase(application);
        platos = database.platoDAO().getByCategoria(categoriaId);
        cliente = database.clienteDAO().getClienteLive();
        executors = new AppExecutors();
        loading = new MutableLiveData<>();
        mensaje = new MutableLiveData<>();
        navigateToDetallePlato = new MutableLiveData<>();
        loading.setValue(false);
        getOrdenesFromService();
    }

    void login(String email, String password) {
        loading.setValue(true);
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(email);
        loginRequest.setPassword(password);
        ApiManager.getApiManagerInstance().login(loginRequest).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    executors.diskIO().execute(() -> {
                        database.clienteDAO().deleteAll();
                        database.clienteDAO().insert(response.body().getData());
                        loading.postValue(false);
                    });
                } else {
                    loading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loading.setValue(false);
                Log.w("TAG", "onFailure: ", t);
            }
        });

    }

    LiveData<Cliente> getCliente() {
        return cliente;
    }

    MutableLiveData<String> getMensaje() {
        return mensaje;
    }

    MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    LiveData<List<Plato>> getPlatos() {
        return platos;
    }

    MutableLiveData<String> getNavigateToDetallePlato() {
        return navigateToDetallePlato;
    }

    private void getOrdenesFromService() {
        loading.setValue(true);
        mensaje.setValue("Estoy trayendo data");
        ApiManager.getApiManagerInstance().getPlatoCategoria(categoriaId).enqueue(new Callback<PlatoCategoriaResponse>() {
            @Override
            public void onResponse(Call<PlatoCategoriaResponse> call, Response<PlatoCategoriaResponse> response) {
                if (response.isSuccessful()) {
                    executors.diskIO().execute(() -> {
                        List<Plato> platoList = response.body().getData();
                        for (Plato plato : platoList) {
                            plato.setCategoriaId(categoriaId);
                        }
                        database.platoDAO().insert(response.body().getData());
                        loading.postValue(false);
                        mensaje.postValue("Terminé de traer data");
                    });
                } else {
                    loading.setValue(false);
                    mensaje.setValue("Error al traer data");
                }
            }

            @Override
            public void onFailure(Call<PlatoCategoriaResponse> call, Throwable t) {
                loading.setValue(false);
                mensaje.setValue("Error al traer data");
                Log.w("TAG", "onFailure: ", t);
            }
        });
    }

    void doneNavigating() {
        navigateToDetallePlato.setValue(null);
    }

    void onPlatoClicked(String idPlato) {
        navigateToDetallePlato.setValue(idPlato);
    }
}
