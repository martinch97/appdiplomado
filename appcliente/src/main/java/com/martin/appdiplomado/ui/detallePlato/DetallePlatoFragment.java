package com.martin.appdiplomado.ui.detallePlato;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.martin.appdiplomado.R;
import com.martin.appdiplomado.ui.base.BaseFragment;
import com.martin.appdiplomado.util.GlideApp;

import java.text.DecimalFormat;

public class DetallePlatoFragment extends BaseFragment {
    private DetallePlatoViewModel viewModel;
    private NavController navController;

    public DetallePlatoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detalle_plato, container, false);

        ImageView ivPlato = root.findViewById(R.id.ivPlato);
        TextView tvPlato = root.findViewById(R.id.tvPlato);
        TextView tvPrecio = root.findViewById(R.id.tvPrecio);
        TextView tvDescripcion = root.findViewById(R.id.tvDescripcion);
        Button btnOrdenar = root.findViewById(R.id.btnOrdenar);
        DecimalFormat decimalFormat = new DecimalFormat("###,###.00");

        String platoId = DetallePlatoFragmentArgs.fromBundle(getArguments()).getPlatoId();

        navController = NavHostFragment.findNavController(this);
        viewModel = ViewModelProviders.of(this, new DetallePlatoFactory(getActivity().getApplication(), platoId)).get(DetallePlatoViewModel.class);
        viewModel.getLoading().observe(this, loading -> {
            if (loading) {
                showProgressDialog();
            } else {
                hideProgressDialog();
            }
        });
        viewModel.getPlato().observe(this, plato -> {
            GlideApp.with(this).load("https://diplomado-restaurant-backend.herokuapp.com/images/platos/" + plato.getImagen()).into(ivPlato);
            tvPlato.setText(plato.getNombre());
            tvDescripcion.setText(plato.getDescripcion());
            Log.d("TAG", "onCreateView: " + plato.getDescripcion());
            tvPrecio.setText("S/." + decimalFormat.format(plato.getPrecio()));
        });
        viewModel.getMensaje().observe(this, mensaje -> Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show());
        btnOrdenar.setOnClickListener(view -> navController.navigate(DetallePlatoFragmentDirections.actionDetallePlatoFragmentToDialogAgregarFragment(platoId)));
        return root;
    }

}
