package com.martin.appdiplomado.ui.categorias;

public interface CategoriaClickListener {
    void onClick(String categoriaId);
}
