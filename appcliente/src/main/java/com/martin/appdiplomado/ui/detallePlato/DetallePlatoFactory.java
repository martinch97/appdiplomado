package com.martin.appdiplomado.ui.detallePlato;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class DetallePlatoFactory implements ViewModelProvider.Factory {
    private Application application;
    private String platoId;

    public DetallePlatoFactory(Application application, String platoId) {
        this.application = application;
        this.platoId = platoId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(DetallePlatoViewModel.class)) {
            return (T) new DetallePlatoViewModel(application, platoId);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
