package com.martin.appdiplomado.ui.platos;

public interface PlatoClickListener {
    void onClick(String platoId);
}
