package com.martin.appdiplomado.ui.categorias;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.martin.appdiplomado.api.ApiManager;
import com.martin.appdiplomado.api.response.CategoriasResponse;
import com.martin.appdiplomado.db.ClienteRoomDatabase;
import com.martin.appdiplomado.db.entities.Categoria;
import com.martin.appdiplomado.util.AppExecutors;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriasViewModel extends AndroidViewModel {
    private ClienteRoomDatabase database;
    private LiveData<List<Categoria>> categorias;
    private AppExecutors executors;
    private MutableLiveData<Boolean> loading;
    private MutableLiveData<String> mensaje;
    private MutableLiveData<String> navigateToPlatos;

    public CategoriasViewModel(@NonNull Application application) {
        super(application);
        database = ClienteRoomDatabase.getDatabase(application);
        categorias = database.categoriaDAO().getAll();
        executors = new AppExecutors();
        loading = new MutableLiveData<>();
        mensaje = new MutableLiveData<>();
        navigateToPlatos = new MutableLiveData<>();
        loading.setValue(false);
        getOrdenesFromService();
    }

    public MutableLiveData<String> getMensaje() {
        return mensaje;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public LiveData<List<Categoria>> getCategorias() {
        return categorias;
    }

    public MutableLiveData<String> getNavigateToPlatos() {
        return navigateToPlatos;
    }

    private void getOrdenesFromService() {
        loading.setValue(true);
        mensaje.setValue("Estoy trayendo data");
        ApiManager.getApiManagerInstance().getCategorias().enqueue(new Callback<CategoriasResponse>() {
            @Override
            public void onResponse(Call<CategoriasResponse> call, Response<CategoriasResponse> response) {
                if (response.isSuccessful()) {
                    executors.diskIO().execute(() -> {
                        database.categoriaDAO().insert(response.body().getData());
                        loading.postValue(false);
                        mensaje.postValue("Terminé de traer data");
                    });
                } else {
                    loading.setValue(false);
                    mensaje.setValue("Error al traer data");
                }
            }

            @Override
            public void onFailure(Call<CategoriasResponse> call, Throwable t) {
                loading.setValue(false);
                mensaje.setValue("Error al traer data");
                Log.w("TAG", "onFailure: ", t);
            }
        });
    }

    public void doneNavigating() {
        navigateToPlatos.setValue(null);
    }

    public void onCategoriaClicked(String idCategoria) {
        navigateToPlatos.setValue(idCategoria);
    }
}
