package com.martin.appdiplomado.ui.dialogAgregar;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.martin.appdiplomado.db.ClienteRoomDatabase;
import com.martin.appdiplomado.db.entities.Orden;
import com.martin.appdiplomado.db.entities.Plato;
import com.martin.appdiplomado.util.AppExecutors;

class DialogAgregarViewModel extends AndroidViewModel {
    private ClienteRoomDatabase database;
    private LiveData<Plato> plato;
    private AppExecutors executors;
    private MutableLiveData<String> mensaje;
    private MutableLiveData<Integer> cantidad;
    private MutableLiveData<Boolean> navigateToPlatos;

    DialogAgregarViewModel(@NonNull Application application, String platoId) {
        super(application);
        database = ClienteRoomDatabase.getDatabase(application);
        plato = database.platoDAO().getPlatoById(platoId);
        executors = new AppExecutors();
        mensaje = new MutableLiveData<>();
        cantidad = new MutableLiveData<>();
        cantidad.setValue(1);
        navigateToPlatos = new MutableLiveData<>();
        navigateToPlatos.setValue(false);
    }

    void sumar() {
        cantidad.setValue(cantidad.getValue() + 1);
    }

    void restar() {
        if (cantidad.getValue() > 1) {
            cantidad.setValue(cantidad.getValue() - 1);
        }
    }

    void guardarOrden() {
        executors.diskIO().execute(() -> {
            Orden orden = new Orden();
            orden.setCantidad(cantidad.getValue());
            orden.setPrecio(plato.getValue().getPrecio());
            orden.setNombre(plato.getValue().getNombre());
            orden.setPlato_id(plato.getValue().get_id());
            database.ordenDAO().insert(orden);
            navigateToPlatos.postValue(true);
        });
    }

    MutableLiveData<Boolean> getNavigateToPlatos() {
        return navigateToPlatos;
    }

    MutableLiveData<Integer> getCantidad() {
        return cantidad;
    }

    LiveData<Plato> getPlato() {
        return plato;
    }

    MutableLiveData<String> getMensaje() {
        return mensaje;
    }
}
