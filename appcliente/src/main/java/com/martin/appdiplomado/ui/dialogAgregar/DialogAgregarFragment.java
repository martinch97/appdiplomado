package com.martin.appdiplomado.ui.dialogAgregar;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.martin.appdiplomado.R;

public class DialogAgregarFragment extends DialogFragment {
    private DialogAgregarViewModel viewModel;
    private NavController navController;

    public DialogAgregarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dialog_agregar, container, false);

        ImageView ivMenos = root.findViewById(R.id.ivMenos);
        ImageView ivMas = root.findViewById(R.id.ivMas);
        TextView tvCantidad = root.findViewById(R.id.tvCantidad);
        Button btnConfirmar = root.findViewById(R.id.btnConfirmar);

        navController = NavHostFragment.findNavController(this);
        viewModel = ViewModelProviders.of(this, new DialogAgregarFactory(getActivity().getApplication(), DialogAgregarFragmentArgs.fromBundle(getArguments()).getPlatoId())).get(DialogAgregarViewModel.class);
        viewModel.getPlato().observe(this, plato -> Toast.makeText(getContext(), plato.toString(), Toast.LENGTH_SHORT).show());
        viewModel.getMensaje().observe(this, mensaje -> Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show());
        viewModel.getCantidad().observe(this, integer -> tvCantidad.setText(integer.toString()));
        viewModel.getNavigateToPlatos().observe(this, aBoolean -> {
            if (aBoolean) {
                navController.navigate(DialogAgregarFragmentDirections.actionDialogAgregarFragmentToCategoriasFragment());
            }
        });
        ivMenos.setOnClickListener(view -> viewModel.restar());
        ivMas.setOnClickListener(view -> viewModel.sumar());
        btnConfirmar.setOnClickListener(view -> viewModel.guardarOrden());
        return root;
    }

}
