package com.martin.appdiplomado.ui.platos;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.martin.appdiplomado.db.entities.Plato;

public class PlatosDiffCallback extends DiffUtil.ItemCallback<Plato> {
    @Override
    public boolean areItemsTheSame(@NonNull Plato oldItem, @NonNull Plato newItem) {
        return oldItem.get_id().equals(newItem.get_id());
    }

    @Override
    public boolean areContentsTheSame(@NonNull Plato oldItem, @NonNull Plato newItem) {
        return oldItem.equals(newItem);
    }
}
