package com.martin.appdiplomado.api.response;

import com.martin.appdiplomado.db.entities.Categoria;

import java.util.List;

public class CategoriasResponse {
    private String message;
    private List<Categoria> data;

    public String getMessage() {
        return message;
    }

    public List<Categoria> getData() {
        return data;
    }
}
