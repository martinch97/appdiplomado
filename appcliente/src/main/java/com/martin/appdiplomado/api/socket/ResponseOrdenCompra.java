package com.martin.appdiplomado.api.socket;

import com.google.gson.annotations.Expose;

public class ResponseOrdenCompra {
    @Expose
    private boolean success;
    @Expose
    private String message;

    @Override
    public String toString() {
        return "ResponseOrdenCompra{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
