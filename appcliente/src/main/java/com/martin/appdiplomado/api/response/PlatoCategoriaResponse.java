package com.martin.appdiplomado.api.response;

import com.martin.appdiplomado.db.entities.Plato;

import java.util.List;

public class PlatoCategoriaResponse {
    private String message;
    private List<Plato> data;

    public String getMessage() {
        return message;
    }

    public List<Plato> getData() {
        return data;
    }
}
