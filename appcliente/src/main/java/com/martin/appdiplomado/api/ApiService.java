package com.martin.appdiplomado.api;

import com.martin.appdiplomado.api.request.LoginRequest;
import com.martin.appdiplomado.api.response.CategoriasResponse;
import com.martin.appdiplomado.api.response.LoginResponse;
import com.martin.appdiplomado.api.response.PlatoCategoriaResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    @POST(ApiConstants.LOGIN)
    Call<LoginResponse> login(@Body LoginRequest request);

    @GET(ApiConstants.GET_CATEGORIAS)
    Call<CategoriasResponse> getCategorias();

    @GET(ApiConstants.GET_PLATO_CATEGORIA)
    Call<PlatoCategoriaResponse> getPlatoCategoria(@Path("categoria") String categoriaId);
}
