package com.martin.appdiplomado.api;

class ApiConstants {
    static final String BASE_URL = "https://diplomado-restaurant-backend.herokuapp.com/";
    static final int TIMEOUT = 60; // secs

    static final String LOGIN = "auth/clientes-login";
    static final String GET_CATEGORIAS = "categorias";
    static final String GET_PLATO_CATEGORIA = "platos/categoria/{categoria}";
}
