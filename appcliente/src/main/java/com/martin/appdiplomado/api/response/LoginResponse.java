package com.martin.appdiplomado.api.response;

import com.martin.appdiplomado.db.entities.Cliente;

public class LoginResponse {
    private String message;
    private Cliente data;

    public String getMessage() {
        return message;
    }

    public Cliente getData() {
        return data;
    }
}
