package com.martin.appdiplomado.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Categoria {
    @NonNull
    @PrimaryKey
    private String _id;
    private String nombre;
    private String descripcion;
    private boolean active;

    public Categoria(@NonNull String _id, String nombre, String descripcion, boolean active) {
        this._id = _id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Categoria categoria = (Categoria) o;

        if (active != categoria.active) return false;
        if (!_id.equals(categoria._id)) return false;
        if (!Objects.equals(nombre, categoria.nombre))
            return false;
        return Objects.equals(descripcion, categoria.descripcion);
    }

    @Override
    public String toString() {
        return "Categoria{" +
                "_id='" + _id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", active=" + active +
                '}';
    }

    @NonNull
    public String get_id() {
        return _id;
    }

    public void set_id(@NonNull String _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
