package com.martin.appdiplomado.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martin.appdiplomado.db.entities.Orden;

import java.util.List;

@Dao
public interface OrdenDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Orden orden);

    @Query("SELECT * FROM orden")
    LiveData<List<Orden>> getAll();

    @Query("DELETE FROM orden")
    void deleteAll();
}
