package com.martin.appdiplomado.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martin.appdiplomado.db.entities.Categoria;

import java.util.List;

@Dao
public interface CategoriaDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Categoria> categoriaList);

    @Query("SELECT * FROM categoria")
    LiveData<List<Categoria>> getAll();

    @Query("DELETE FROM categoria")
    void deleteAll();
}
