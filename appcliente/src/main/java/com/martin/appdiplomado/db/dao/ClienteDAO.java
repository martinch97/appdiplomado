package com.martin.appdiplomado.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martin.appdiplomado.db.entities.Cliente;

@Dao
public interface ClienteDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Cliente cliente);

    @Query("SELECT * FROM cliente LIMIT 1")
    LiveData<Cliente> getClienteLive();

    @Query("SELECT _id FROM cliente LIMIT 1")
    String getClienteId();

    @Query("DELETE FROM cliente")
    void deleteAll();
}
