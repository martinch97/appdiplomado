package com.martin.appdiplomado.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;

import java.util.Objects;

@Entity
public class Orden {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int _id;
    @Expose
    private String plato_id;
    @Expose
    private String nombre;
    @Expose
    private double precio;
    @Expose
    private double cantidad;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orden orden = (Orden) o;

        if (_id != orden._id) return false;
        if (Double.compare(orden.precio, precio) != 0) return false;
        if (Double.compare(orden.cantidad, cantidad) != 0) return false;
        if (!Objects.equals(plato_id, orden.plato_id))
            return false;
        return Objects.equals(nombre, orden.nombre);
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public String toString() {
        return "Orden{" +
                "_id=" + _id +
                ", plato_id='" + plato_id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", precio=" + precio +
                ", cantidad=" + cantidad +
                '}';
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getPlato_id() {
        return plato_id;
    }

    public void setPlato_id(String plato_id) {
        this.plato_id = plato_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
}
