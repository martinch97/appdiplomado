package com.martin.appdiplomado.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Plato {
    @NonNull
    @PrimaryKey
    private String _id;
    private String imagen;
    private String nombre;
    private double precio;
    private String descripcion;
    private String categoriaId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Plato plato = (Plato) o;

        if (Double.compare(plato.precio, precio) != 0) return false;
        if (!_id.equals(plato._id)) return false;
        if (!Objects.equals(imagen, plato.imagen)) return false;
        if (!Objects.equals(nombre, plato.nombre)) return false;
        if (!Objects.equals(descripcion, plato.descripcion))
            return false;
        return Objects.equals(categoriaId, plato.categoriaId);
    }

    @Override
    public String toString() {
        return "Plato{" +
                "_id='" + _id + '\'' +
                ", imagen='" + imagen + '\'' +
                ", nombre='" + nombre + '\'' +
                ", precio=" + precio +
                ", descripcion='" + descripcion + '\'' +
                ", categoriaId='" + categoriaId + '\'' +
                '}';
    }

    @NonNull
    public String get_id() {
        return _id;
    }

    public void set_id(@NonNull String _id) {
        this._id = _id;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(String categoriaId) {
        this.categoriaId = categoriaId;
    }
}
