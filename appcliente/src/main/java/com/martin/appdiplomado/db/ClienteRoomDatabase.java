package com.martin.appdiplomado.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.martin.appdiplomado.db.dao.CategoriaDAO;
import com.martin.appdiplomado.db.dao.ClienteDAO;
import com.martin.appdiplomado.db.dao.OrdenDAO;
import com.martin.appdiplomado.db.dao.PlatoDAO;
import com.martin.appdiplomado.db.entities.Categoria;
import com.martin.appdiplomado.db.entities.Cliente;
import com.martin.appdiplomado.db.entities.Orden;
import com.martin.appdiplomado.db.entities.Plato;

@Database(entities = {
        Categoria.class,
        Cliente.class,
        Orden.class,
        Plato.class
}, version = 10, exportSchema = false)
public abstract class ClienteRoomDatabase extends RoomDatabase {
    private static volatile ClienteRoomDatabase INSTANCE;

    public static ClienteRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ClienteRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room
                            .databaseBuilder(context.getApplicationContext(), ClienteRoomDatabase.class, "cliente_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract CategoriaDAO categoriaDAO();

    public abstract ClienteDAO clienteDAO();

    public abstract PlatoDAO platoDAO();

    public abstract OrdenDAO ordenDAO();

}
