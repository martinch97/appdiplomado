package com.martin.appdiplomado.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martin.appdiplomado.db.entities.Plato;

import java.util.List;

@Dao
public interface PlatoDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Plato> PlatoList);

    @Query("SELECT * FROM Plato")
    LiveData<List<Plato>> getAll();

    @Query("SELECT * FROM Plato WHERE categoriaId = :categoriaId")
    LiveData<List<Plato>> getByCategoria(String categoriaId);

    @Query("SELECT * FROM Plato WHERE _id = :platoId")
    LiveData<Plato> getPlatoById(String platoId);

    @Query("DELETE FROM Plato")
    void deleteAll();
}
